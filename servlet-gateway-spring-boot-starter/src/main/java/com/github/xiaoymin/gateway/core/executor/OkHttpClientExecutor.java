/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.github.xiaoymin.gateway.core.executor;


import com.github.xiaoymin.gateway.core.RouteExecutor;
import com.github.xiaoymin.gateway.core.RouteRequestContext;
import com.github.xiaoymin.gateway.core.RouteResponse;

/***
 *
 * @since:servlet-gateway-spring-boot-starter 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2020/10/29 20:40
 */
public class OkHttpClientExecutor implements RouteExecutor {
    @Override
    public RouteResponse executor(RouteRequestContext routeContext) {
        return null;
    }
}
