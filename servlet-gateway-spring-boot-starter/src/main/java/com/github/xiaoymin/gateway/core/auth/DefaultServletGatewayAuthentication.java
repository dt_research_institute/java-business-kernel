/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.github.xiaoymin.gateway.core.auth;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a>
 * 2021/02/02 8:55
 * @since:servlet-gateway-spring-boot-starter 1.0
 */
public class DefaultServletGatewayAuthentication implements ServletGatewayAuthentication{

    @Override
    public boolean auth(ServletRequest request, ServletResponse response) {
        return false;
    }

    @Override
    public void failedHandle(ServletRequest request, ServletResponse response) {

    }

    @Override
    public boolean required() {
        //don't need auth
        return false;
    }
}
