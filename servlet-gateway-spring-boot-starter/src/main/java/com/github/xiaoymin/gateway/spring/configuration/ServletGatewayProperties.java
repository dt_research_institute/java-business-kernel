/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.github.xiaoymin.gateway.spring.configuration;

import cn.hutool.core.collection.CollectionUtil;
import com.github.xiaoymin.gateway.spring.support.CloudSetting;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a>
 * 2020/11/13 13:12
 * @since:servlet-gateway-spring-boot-starter 1.0
 */
@Component
@ConfigurationProperties(prefix = "server.servlet.gateway")
public class ServletGatewayProperties {

    /**
     * 是否开启聚合模式
     */
    private boolean enable=false;
    /**
     * 路由匹配规则
     */
    private List<String> urlPatterns= CollectionUtil.newArrayList("/*");

    /**
     * 任意聚合
     */
    private CloudSetting cloud;


    public boolean isEnable() {
        return enable;
    }

    public void setEnable(boolean enable) {
        this.enable = enable;
    }

    public CloudSetting getCloud() {
        return cloud;
    }

    public void setCloud(CloudSetting cloud) {
        this.cloud = cloud;
    }

    public List<String> getUrlPatterns() {
        return urlPatterns;
    }

    public void setUrlPatterns(List<String> urlPatterns) {
        this.urlPatterns = urlPatterns;
    }
}
