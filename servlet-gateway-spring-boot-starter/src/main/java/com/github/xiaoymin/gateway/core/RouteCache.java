/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.github.xiaoymin.gateway.core;

/***
 *
 * @since:servlet-gateway-spring-boot-starter 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2020/10/31 10:56
 */
public interface RouteCache<K,V> {

    /**
     * 添加缓存
     * @param k 键
     * @param v 值
     * @return 是否成功
     */
    boolean put(K k,V v);

    /**
     * 获取缓存值
     * @param k 键值
     * @return 示例
     */
    V get(K k);

    /**
     * 移除缓存
     * @param k 键值
     * @return 布尔值
     */
    boolean remove(K k);
}
