/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.github.xiaoymin.gateway.core;

import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Map;

/***
 *
 * @since:servlet-gateway-spring-boot-starter 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2020/10/29 21:58
 */
public interface RouteResponse {
    /**
     * 获取响应头
     * @return 所有请求Header
     */
    Map<String,String> getHeaders();

    /**
     * 是否请求成功
     * @return 布尔值
     */
    boolean success();

    /**
     * 获取响应状态码
     * @return HTTP状态码
     */
    int getStatusCode();

    /**
     * 获取响应类型
     * @return 响应类型
     */
    String getContentType();

    /**
     * 响应内容长度
     * @return 内容长度
     */
    Long getContentLength();

    /**
     * 获取encoding
     * @return 编码
     */
    Charset getCharsetEncoding();

    /**
     * 响应实体
     * @return 响应内容
     */
    InputStream getBody();

    /**
     * 获取text返回
     * @return 文本内容
     */
    String text();

}
