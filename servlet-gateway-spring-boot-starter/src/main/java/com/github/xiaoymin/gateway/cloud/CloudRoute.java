/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.github.xiaoymin.gateway.cloud;


import cn.hutool.crypto.digest.MD5;
import com.github.xiaoymin.gateway.core.common.RouteModeEnum;

import java.util.Map;

/**
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a>
 * 2020/11/13 13:14
 * @since:servlet-gateway-spring-boot-starter 1.0
 */
public class CloudRoute{

    /**
     * 转发模式
     */
    private RouteModeEnum mode;
    /**
     * 匹配值
     */
    private String value;

    /**
     * 转发目标地址，例如：http://192.179.0.1:8999
     */
    private String uri;

    /**
     * 发送请求头
     */
    private Map<String,String> headers;

    public String pkId(){
        return MD5.create().digestHex(this.toString());
    }


    @Override
    public String toString() {
        return "CloudRoute{" +
                "mode=" + mode +
                ", value='" + value + '\'' +
                ", uri='" + uri + '\'' +
                '}';
    }


    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public RouteModeEnum getMode() {
        return mode;
    }

    public void setMode(RouteModeEnum mode) {
        this.mode = mode;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
