/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */
package com.github.xiaoymin.gateway.core.auth;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * 在请求转发代理之前进行必要的权限校验
 * <p>
 * 客户端组件必须实现该接口,并且注入到Spring的容器中
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a>
 * 2021/02/01 10:58
 * @since:ots-cloud 1.0
 */
public interface ServletGatewayAuthentication {

    /**
     * 权限校验
     * @param request 请求request对象
     * @param response 响应对象
     * @return 是否权限校验通过
     */
    boolean auth(ServletRequest request, ServletResponse response);

    /**
     * 权限校验失败后的处理逻辑
     * @param request 请求对象
     * @param response 响应对象
     */
    void failedHandle(ServletRequest request, ServletResponse response);

    /**
     * 是否需要鉴权，默认true
     * @return 是否需要鉴权
     */
    default boolean required(){return true;}
}
