/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.github.xiaoymin.gateway.core;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.io.IoUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONObject;
import com.github.xiaoymin.gateway.core.executor.ApacheClientExecutor;
import com.github.xiaoymin.gateway.core.executor.OkHttpClientExecutor;
import com.github.xiaoymin.gateway.core.common.ExecutorEnum;
import com.github.xiaoymin.gateway.core.common.RouteModeEnum;
import com.github.xiaoymin.gateway.core.pojo.ServiceRoute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URI;
import java.util.*;

/***
 *
 * @since:servlet-gateway-spring-boot-starter 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2020/10/29 20:08
 */
public class RouteDispatcher {
    /**
     * 请求头
     */
    public static final String ROUTE_PROXY_HEADER_NAME="knfie4j-gateway-request";
    public static final String ROUTE_PROXY_HEADER_BASIC_NAME="knife4j-gateway-basic-request";
    public static final String OPENAPI_GROUP_ENDPOINT="/swagger-resources";
    public static final String OPENAPI_GROUP_INSTANCE_ENDPOINT="/swagger-instance";
    public static final String ROUTE_BASE_PATH="/";

    Logger logger= LoggerFactory.getLogger(RouteDispatcher.class);
    /**
     * 当前项目的contextPath
     */
    private String rootPath;

    private RouteRepository routeRepository;

    private RouteExecutor routeExecutor;

    private RouteCache<String, ServiceRoute> routeCache;

    private Set<String> ignoreHeaders=new HashSet<>();

    public RouteDispatcher(RouteRepository routeRepository, RouteCache<String, ServiceRoute> routeRouteCache, ExecutorEnum executorEnum, String rootPath){
        this.routeRepository=routeRepository;
        this.routeCache=routeRouteCache;
        this.rootPath=rootPath;
        initExecutor(executorEnum);
        ignoreHeaders.addAll(Arrays.asList(new String[]{
                "host","content-length",ROUTE_PROXY_HEADER_NAME,ROUTE_PROXY_HEADER_BASIC_NAME,"Request-Origion"
        }));
    }

    private void initExecutor(ExecutorEnum executorEnum){
        if (executorEnum==null){
            throw new IllegalArgumentException("ExecutorEnum can not be empty");
        }
        switch (executorEnum){
            case APACHE:
                this.routeExecutor=new ApacheClientExecutor();
                break;
            case OKHTTP:
                this.routeExecutor=new OkHttpClientExecutor();
                break;
            default:
                throw new UnsupportedOperationException("UnSupported ExecutorType:"+executorEnum.name());
        }
    }

    public Optional<ServiceRoute> assertServletRequest(HttpServletRequest request){
        return this.routeRepository.applyServiceRoute(request);
    }

    public void execute(HttpServletRequest request, HttpServletResponse response,ServiceRoute serviceRoute){
        try{
           RouteRequestContext routeContext=new RouteRequestContext();
           this.buildContext(routeContext,request,serviceRoute);
           RouteResponse routeResponse=routeExecutor.executor(routeContext);
           writeResponseHeader(routeResponse,response);
           writeBody(routeResponse,response);
        }catch (Exception e){
           logger.error("has Error:{}",e.getMessage());
           logger.error(e.getMessage(),e);
           //write Default
            writeDefault(request,response,e.getMessage());
        }
    }

    protected void writeDefault(HttpServletRequest request,HttpServletResponse response,String errMsg){
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        try {
            PrintWriter printWriter=response.getWriter();
            Map<String,String> map= new HashMap<>();
            map.put("message",errMsg);
            map.put("code","500");
            map.put("path",request.getRequestURI());
            new JSONObject(map).write(printWriter);
            printWriter.close();
        } catch (IOException e) {
            //ignore
        }
    }

    /**
     * Write响应头
     * @param routeResponse 响应路由
     * @param response 响应对象
     */
    protected void writeResponseHeader(RouteResponse routeResponse,HttpServletResponse response){
        if (routeResponse!=null){
            if (CollectionUtil.isNotEmpty(routeResponse.getHeaders())){
                for (Map.Entry<String,String> entry:routeResponse.getHeaders().entrySet()){
                    //logger.info("{}:{}",entry.getKey(),entry.getValue());
                    if (!StrUtil.equalsIgnoreCase(entry.getKey(),"Transfer-Encoding")){
                        response.addHeader(entry.getKey(),entry.getValue());
                    }
                }
            }
            logger.info("响应类型:{},响应编码:{}",routeResponse.getContentType(),routeResponse.getCharsetEncoding());
            response.setContentType(routeResponse.getContentType());
            if (routeResponse.getContentLength()>0){
                response.setContentLengthLong(routeResponse.getContentLength());
            }
            response.setCharacterEncoding(routeResponse.getCharsetEncoding().displayName());
        }
    }

    /**
     * 响应内容
     * @param routeResponse 路由响应
     * @param response 响应
     * @throws IOException 抛出IO异常
     */
    protected void writeBody(RouteResponse routeResponse,HttpServletResponse response) throws IOException {
        if (routeResponse!=null){
            if (routeResponse.success()){
                InputStream inputStream=routeResponse.getBody();
                if (inputStream!=null){
                    int read=-1;
                    byte[] bytes=new byte[1024*1024];
                    ServletOutputStream outputStream=response.getOutputStream();
                    while ((read=inputStream.read(bytes))!=-1){
                        outputStream.write(bytes,0,read);
                    }
                    IoUtil.close(inputStream);
                    IoUtil.close(outputStream);
                }
            }else{
                String text=routeResponse.text();
                if (StrUtil.isNotBlank(text)){
                    PrintWriter printWriter=response.getWriter();
                    printWriter.write(text);
                    printWriter.close();
                }
            }

        }
    }

    /**
     * 构建路由的请求上下文
     * @param routeRequestContext 请求上下文对象
     * @param request 请求
     * @param serviceRoute 路由实例
     * @throws IOException IO异常
     */
    protected void buildContext(RouteRequestContext routeRequestContext,HttpServletRequest request,ServiceRoute serviceRoute) throws IOException {
        //String uri="http://knife4j.xiaominfo.com";
        String uri=serviceRoute.getUri();
        if (StrUtil.isBlank(uri)){
            throw new RuntimeException("Uri is Empty");
        }
        String host=URI.create(uri).getHost();
        String fromUri=request.getRequestURI();
        StringBuilder requestUrlBuilder=new StringBuilder();
        requestUrlBuilder.append(uri);
        //判断当前聚合项目的contextPath
        if (StrUtil.isNotBlank(this.rootPath)&&!StrUtil.equals(this.rootPath,ROUTE_BASE_PATH)){
            fromUri=fromUri.replaceFirst(this.rootPath,"");
        }
        if (serviceRoute.getMode()== RouteModeEnum.ROUTE_MODE_PREFIX){
            //前缀转发，替换
            fromUri=fromUri.replaceFirst(serviceRoute.getValue(),"/");
        }
        if (!StrUtil.startWith(fromUri,"/")){
            requestUrlBuilder.append("/");
        }
        requestUrlBuilder.append(fromUri);
        //String requestUrl=uri+fromUri;
        String requestUrl=requestUrlBuilder.toString();
        logger.info("目标请求Url:{},请求类型:{},Host:{}",requestUrl,request.getMethod(),host);
        routeRequestContext.setOriginalUri(fromUri);
        routeRequestContext.setUrl(requestUrl);
        routeRequestContext.setMethod(request.getMethod());
        Enumeration<String> enumeration=request.getHeaderNames();
        while (enumeration.hasMoreElements()){
            String key=enumeration.nextElement();
            String value=request.getHeader(key);
            if (!ignoreHeaders.contains(key.toLowerCase())){
                routeRequestContext.addHeader(key,value);
            }
        }
        //是否有默认Header需要发送
        if (CollectionUtil.isNotEmpty(serviceRoute.getHeaders())){
            for (Map.Entry<String,String> entry:serviceRoute.getHeaders().entrySet()){
                routeRequestContext.addHeader(entry.getKey(),entry.getValue());
            }
        }
        routeRequestContext.addHeader("Host",host);
        Enumeration<String> params=request.getParameterNames();
        while (params.hasMoreElements()){
            String name=params.nextElement();
            String value=request.getParameter(name);
            //logger.info("param-name:{},value:{}",name,value);
            routeRequestContext.addParam(name,value);
        }
        routeRequestContext.setRequestContent(request.getInputStream());
    }


    public List<ServiceRoute> getRoutes(){
        return routeRepository.getRoutes();
    }
}
