/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */
package com.github.xiaoymin.gateway.core;


import com.github.xiaoymin.gateway.core.pojo.ServiceRoute;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

/***
 *
 * @since:servlet-gateway-spring-boot-starter 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2020/10/29 20:09
 */
public interface RouteRepository {

    String ROUTE_MODE_HEADER_NAME="ots-cloud-gateway";

    /**
     * 获取所有
     * @return 获取所有路由实例
     */
    List<ServiceRoute> getRoutes();

    /**
     * 校验判断当前请求是否需要转发
     * @param httpServletRequest 请求头
     * @return 是否转发标志为
     */
    boolean checkRouteProxy(HttpServletRequest httpServletRequest);

    /**
     * 根据请求头获取当前请求是否是转发Route
     * @param httpServletRequest 请求对象
     * @return ServiceRoute对象
     */
    Optional<ServiceRoute> applyServiceRoute(HttpServletRequest httpServletRequest);

}
