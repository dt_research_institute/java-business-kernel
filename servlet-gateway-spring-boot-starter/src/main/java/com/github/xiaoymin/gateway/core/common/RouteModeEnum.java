/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.github.xiaoymin.gateway.core.common;

/**
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a>
 * 2021/01/11 16:48
 * @since:ots-cloud 1.0
 */
public enum  RouteModeEnum {

    ROUTE_MODE_HEADER("ROUTE_MODE_HEADER","基于请求Header模式"),
    ROUTE_MODE_PREFIX("ROUTE_MODE_PREFIX","基于前缀匹配模式"),
    ROUTE_MODE_SUFFIX("ROUTE_MODE_SUFFIX","基于后缀匹配模式");

    private String code;
    private String label;

    RouteModeEnum(String code, String label) {
        this.code=code;
        this.label=label;
    }

    public String getCode() {
        return code;
    }

    public String getLabel() {
        return label;
    }
}
