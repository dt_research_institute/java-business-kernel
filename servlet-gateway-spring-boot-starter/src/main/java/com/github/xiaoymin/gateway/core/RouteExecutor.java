/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */
package com.github.xiaoymin.gateway.core;

/***
 *
 * @since:servlet-gateway-spring-boot-starter 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2020/10/29 20:33
 */
public interface RouteExecutor {

    /**
     * 执行器
     * @param routeContext 请求上下文
     * @return 执行器结果响应对象
     */
    RouteResponse executor(RouteRequestContext routeContext);
}
