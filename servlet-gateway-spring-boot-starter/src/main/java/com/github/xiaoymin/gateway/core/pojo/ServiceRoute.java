/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */

package com.github.xiaoymin.gateway.core.pojo;

import cn.hutool.core.util.ReUtil;
import cn.hutool.core.util.StrUtil;
import com.github.xiaoymin.gateway.core.common.RouteModeEnum;
import com.github.xiaoymin.gateway.cloud.CloudRoute;

import java.util.Map;

/***
 * 最终返回前端Swagger的数据结构
 * @since:servlet-gateway-spring-boot-starter 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2020/10/31 9:34
 */
public class ServiceRoute {

    /**
     * 转发模式
     */
    private RouteModeEnum mode;
    /**
     * 匹配值
     */
    private String value;

    /**
     * 转发目标地址，例如：http://192.179.0.1:8999
     */
    private String uri;

    /**
     * 发送请求头
     */
    private Map<String,String> headers;


    /**
     * 根据Cloud配置创建
     * @param cloudRoute 云端配置
     */
    public ServiceRoute(CloudRoute cloudRoute){
        if (cloudRoute!=null){
           this.mode=cloudRoute.getMode();
           this.value=cloudRoute.getValue();
           this.headers=cloudRoute.getHeaders();
            if (StrUtil.isNotBlank(cloudRoute.getUri())){
                //判断
                if (!ReUtil.isMatch("(http|https)://.*?$",cloudRoute.getUri())){
                    this.uri="http://"+cloudRoute.getUri();
                }else{
                    this.uri=cloudRoute.getUri();
                }
            }
        }
    }

    public RouteModeEnum getMode() {
        return mode;
    }

    public void setMode(RouteModeEnum mode) {
        this.mode = mode;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }


    public Map<String, String> getHeaders() {
        return headers;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }
}
