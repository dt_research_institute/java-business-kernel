/*
 * Copyright (C) 2018 Zhejiang xiaominfo Technology CO.,LTD.
 * All rights reserved.
 * Official Web Site: http://www.xiaominfo.com.
 * Developer Web Site: http://open.xiaominfo.com.
 */
package com.github.xiaoymin.gateway.core.cache;

import com.github.xiaoymin.gateway.core.RouteCache;
import com.github.xiaoymin.gateway.core.pojo.ServiceRoute;

import java.util.concurrent.ConcurrentHashMap;
/***
 *
 * @since:servlet-gateway-spring-boot-starter 1.0
 * @author <a href="mailto:xiaoymin@foxmail.com">xiaoymin@foxmail.com</a> 
 * 2020/10/31 11:07
 */
public class RouteInMemoryCache implements RouteCache<String, ServiceRoute> {

    private final ConcurrentHashMap<String, ServiceRoute> cache=new ConcurrentHashMap<>();

    @Override
    public boolean put(String s, ServiceRoute swaggerRoute) {
        cache.put(s,swaggerRoute);
        return true;
    }

    @Override
    public ServiceRoute get(String s) {
        return cache.get(s);
    }

    @Override
    public boolean remove(String s) {
        cache.remove(s);
        return true;
    }
}
